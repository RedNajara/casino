$(document).ready(function () {

    //Slick slider
    $('.b-slider').slick({
        centerMode:true,
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: true,
        infinite: false,
        cssEase: 'linear',
        variableWidth: true,
        variableHeight: true,
        arrows: false
    });


    //Show mobile menu
    $('.b-icon-open').click(function(){
        $('.modal-backdrop').show();
        $('.b-sidebar__admin-panel').find('.b-sidebar--wrapper').addClass('no-transform');
        $('body').css('overflow', 'hidden');
    });

    $('.modal-backdrop').click(function(){
        $('.b-sidebar--wrapper').removeClass('no-transform');
        $('.modal-backdrop').hide();
        $('body').css('overflow', 'visible');
    });

    $('.mobile-winner').click(function(){
        $('.modal-backdrop').show();
        $('.b-aside').find('.b-sidebar--wrapper').addClass('no-transform');
        $('body').css('overflow', 'hidden');
    });

    //Show popup
    $('.button--create-account').click(function(){
        $('.b-popup').show();
        $('.popup-backdrop').show();
        $('body').css('overflow', 'hidden');
    });

    //Show support
    $('.button--support').click(function(){
        $('.b-support').show();
    });

    $('.b-support--close').click(function(){
        $('.b-support').hide();
    });

});

$('select').each(function(){
    var $this = $(this), numberOfOptions = $(this).children('option').length;

    $this.addClass('select-hidden');
    $this.wrap('<div class="select"></div>');
    $this.after('<div class="select-styled"></div>');

    var $styledSelect = $this.next('div.select-styled');
    $styledSelect.text($this.children('option').eq(0).text());

    var $list = $('<ul />', {
        'class': 'select-options'
    }).insertAfter($styledSelect);

    for (var i = 0; i < numberOfOptions; i++) {
        $('<li />', {
            text: $this.children('option').eq(i).text(),
            rel: $this.children('option').eq(i).val()
        }).appendTo($list);
    }

    var $listItems = $list.children('li');

    $styledSelect.click(function(e) {
        e.stopPropagation();
        $('div.select-styled.active').not(this).each(function(){
            $(this).removeClass('active').next('ul.select-options').hide();
        });
        $(this).toggleClass('active').next('ul.select-options').toggle();
    });

    $listItems.click(function(e) {
        e.stopPropagation();
        $styledSelect.text($(this).text()).removeClass('active');
        $this.val($(this).attr('rel'));
        $list.hide();
        //console.log($this.val());
    });

    $(document).click(function() {
        $styledSelect.removeClass('active');
        $list.hide();
    });

});