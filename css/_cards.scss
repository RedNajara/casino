.b-cards {
  max-width: $card-block-max-width;
  padding-top: 33px;
  padding-left: 10px;
  padding-right: 10px;

  @media only screen and (max-width: $breakpoint-md){
    max-width: $card-block-max-width-md;
    padding: $padding-md;
  }
}

.grid-layout {
  display: grid;
  grid-template-columns: repeat(auto-fill, minmax(229px, 1fr));
  grid-gap: 6px;
  grid-auto-rows: minmax(154px, auto);
  grid-auto-flow: dense;
  padding: 0;

  @media only screen and (max-width: $breakpoint-lg){
    grid-template-columns: repeat(auto-fill, minmax(140px, 1fr));
    grid-gap: 3px;
    grid-auto-rows: minmax(92px, auto);
  }

  &--item {
    border-radius: 8px;
    line-height: 0;
    position: relative;

    & .button {
      position: absolute;
      top: 50%;
      left: 50%;
      @include translate(-50%, -50%);
      display: none;
    }

    &-vertical {
      grid-column-end: span 1;
      grid-row-end: span 2;
    }

    &-horizontal {
      grid-column-end: span 2;
      grid-row-end: span 1;
    }
  }

  &--image {
    border: 1px solid $card-border-color;
    border-radius: $card-border-radius;
    width: 100%;
    height: 100%;
  }

  &--link {
    position: absolute;
    display: block;
    top: 0;
    left: 0;
    bottom: 0;
    right: 0;

    &:before {
      content: "";
      position: absolute;
      top: 0;
      left: 0;
      width: 100%;
      height: 100%;
      @include box-shadow(inset 0 1px 31px $card-box-shadow-color);
      border-radius: $card-border-radius;
      background-color: $card-hover-background-color;
      opacity: 0.92;
      display: none;
    }

    &:hover {
      &:before {
        display: block;
      }

      & .button {
        display: block;
      }
    }

    @media only screen and (max-width: $breakpoint-lg){
      &:before {
        display: none;
      }

      & .button {
        display: none;
      }

      &:active {
        &:before {
          display: none;
        }

        & .button {
          display: none;
        }
      }

      &:hover {
        &:before {
          display: none;
        }

        & .button {
          display: none;
        }
      }
    }
  }
}